﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;
namespace WineStoreBUS.BUS
{
    public class OrderDetails
    {
        public static int AutoID()
        {
            var db = new WineStoreConnectionDB();
            int a = db.SingleOrDefault<int>("select top 1 MaChiTietDonDatHang from ChiTietDonDatHang order by MaChiTietDonDatHang desc");
            if (a == null)
            {
                a = 0;

            }
            return a + 1;



        }

        public static IEnumerable<ChiTietDonDatHang> LoadOrderDetailById(string id)
        {
            var data = new WineStoreConnectionDB();
            IEnumerable<ChiTietDonDatHang > ds = data.Query<ChiTietDonDatHang>("Select * from ChiTietDonDatHang Where MaDonDatHang = @0",id);

            return ds;

        }
        public static void Insert(ChiTietDonDatHang order)
        {

            var db = new WineStoreConnectionDB();


            db.Insert("ChiTietDonDatHang", "MaChiTietDonDatHang", order);
        }
    }
}
