﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;
namespace WineStoreBUS.BUS
{
    public class LoaiSPBUS
    {
        public static IEnumerable<LoaiSanPham> List()
        {
            var db = new WineStoreConnectionDB();
            return db.Query<LoaiSanPham>("Select * from LoaiSanPham where BiXoa = 0");
        }
       
        public static PetaPoco.Page<LoaiSanPham> DanhSach(int page)
        {
            var db = new WineStoreConnectionDB();
            var result = db.Page<LoaiSanPham>(page, 6,"SELECT * FROM LoaiSanPham where BiXoa = 0");

            return result;
        }

        public static int AutoID()
        {
            var db = new WineStoreConnectionDB();
            int a = db.SingleOrDefault<int>("select top 1 MaLoaiSanPham from LoaiSanPham order by MaLoaiSanPham desc");

            return a + 1;



        }
        public static bool Update(LoaiSanPham tk)
        {
            try
            {
                var db = new WineStoreConnectionDB();
                db.Update("LoaiSanPham", "MaLoaiSanPham", tk);
                return true;

            }
            catch (Exception e)
            {
                return false;

            }


        }
        public static bool Create(LoaiSanPham tk)
        {

            try
            {


                var db = new WineStoreConnectionDB();
                int id = AutoID();
                tk.MaLoaiSanPham = id;
                db.Insert("LoaiSanPham", tk);

                return true;
            }
            catch (Exception e)
            {

                return false;



            }



        } 
        public static String NameLoaiSPByID(int id)
        {
            var db = new WineStoreConnectionDB();
            return db.SingleOrDefault<String>("Select TenLoaiSanPham from LoaiSanPham where MaLoaiSanPham=@0 and BiXoa =0", id);
        }
        public static LoaiSanPham LoadCatelogyByID(int id)
        {
            var db = new WineStoreConnectionDB();
            return db.SingleOrDefault<LoaiSanPham>("Select * from LoaiSanPham where MaLoaiSanPham=@0 and BiXoa =0", id);
        }
    }
}
