﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;
namespace WineStoreBUS.BUS
{
    public class ProducerBUS
    {
        public static IEnumerable<HangSanXuat> List()
        {
            var db = new WineStoreConnectionDB();
            return db.Query<HangSanXuat>("Select * from HangSanXuat where BiXoa = 0");
        }
        public static IEnumerable<HangSanXuat> ListManager()
        {
            var db = new WineStoreConnectionDB();
            return db.Query<HangSanXuat>("Select * from HangSanXuat ");
        }
        public static String NameProducerByID(int id)
        {
            var db = new WineStoreConnectionDB();
            return db.SingleOrDefault<String>("Select TenHangSanXuat from HangSanXuat where MaHangSanXuat=@0 and BiXoa =0",id);
        }
        public static HangSanXuat LoadProducerById(int id)
        {
            var db = new WineStoreConnectionDB();
            return db.SingleOrDefault<HangSanXuat>("Select * from HangSanXuat where MaHangSanXuat=@0 ", id);
        }
        public static PetaPoco.Page<HangSanXuat> DanhSach(int page)
        {
            var db = new WineStoreConnectionDB();
            var result = db.Page<HangSanXuat>(page, 6, // <-- page number and items per page

        "Select * from HangSanXuat where BiXoa = 0");

            return result;
        }
        public static int AutoID()
        {
            var db = new WineStoreConnectionDB();
            int a = db.SingleOrDefault<int>("select top 1 MaHangSanXuat from HangSanXuat order by MaHangSanXuat desc");

            return a + 1;



        }
        public static bool Update(HangSanXuat tk)
        {
            try
            {
                var db = new WineStoreConnectionDB();
                db.Update("HangSanXuat", "MaHangSanXuat", tk);
                return true;

            }
            catch (Exception e)
            {
                return false;

            }
          

        }
        public static bool Create(HangSanXuat tk)
        {

            try
            {


                var db = new WineStoreConnectionDB();
               
                
                int id = AutoID();
                tk.MaHangSanXuat = id;
                db.Insert("HangSanXuat", tk);

                return true;
            }
            catch (Exception e)
            {

                return false;



            }



        } 
    }
}
