﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;
namespace WineStoreBUS.BUS
{
   public class ProductBUS
   {

       public static PetaPoco.Page<SanPham> SearchBar(int CatId, int ManuId,int money,int page)
       {
           string Tien = "" ;

           var db = new WineStoreConnectionDB();
          String catid,manuid;


          if (CatId == 0)
              catid = "MaLoaiSanPham = MaLoaiSanPham ";
          else
              catid = string.Format(" MaLoaiSanPham = {0}", CatId);

          if (ManuId == 0)
              manuid = " MaHangSanXuat = MaHangSanXuat ";
          else
              manuid = string.Format(" MaHangSanXuat={0} ", ManuId);

       
        switch (money) {

            case 1: Tien = "  GiaSanPham BETWEEN 0 AND 1000000 "; break;
            case 2: Tien = "  GiaSanPham BETWEEN 1000001 AND 5000000 "; break;
            case 3: Tien = "  GiaSanPham BETWEEN 5000000 AND 10000000 "; break;
            case 4: Tien = "  GiaSanPham >10000000"; break;
            case 0: Tien = "  GiaSanPham >0"; break;  
        }
        string a = "select * from SanPham where " + catid + "AND "+ manuid + " and BiXoa = 0 and" + Tien;
      



        var result = db.Page<SanPham>(page, 12,a);

           return result;
       }
       public static PetaPoco.Page<SanPham> ListSearchFound(string keyword,int page)
       {

           var db = new WineStoreConnectionDB();
    
           string key = keyword.ToLower();


           var result = db.Page<SanPham>(page, 12, "select * from SanPham where  LOWER(TenSanPham) like N'%"+key+"%'");
           
           return result;
       }
       public static SanPham GetbyID(int id)
       {
           var db = new WineStoreConnectionDB();
           return db.SingleOrDefault<SanPham>("select * from SanPham where MaSanPham = @0 ",id);
       }

       public static IEnumerable<SanPham> ListNew()
       {
           var db = new WineStoreConnectionDB();
           return db.Query<SanPham>("select top 10 * from SanPham where BiXoa = 0 order by NgayLap DESC");
       }
       public static IEnumerable<SanPham> ListSearch(string a)
       {
           var db = new WineStoreConnectionDB();
           
           return db.Query<SanPham>("Select * from SanPham where BiXoa=0").Where(m=>m.TenSanPham.ToLower().Contains(a.ToLower()));
       }
       public static IEnumerable<SanPham> ListSelling()
       {
           var db = new WineStoreConnectionDB();
           return db.Query<SanPham>("select top 10 * from SanPham where BiXoa = 0 order by SoLuongBan DESC");
       }
       public static IEnumerable<SanPham> ListTopView()
       {
           var db = new WineStoreConnectionDB();
           return db.Query<SanPham>("select top 10 * from SanPham where BiXoa = 0  order by SoLuotXem DESC");
       }
       public static IEnumerable<SanPham> ListTop5ByProducer(int id)
       {
           var db = new WineStoreConnectionDB();
           return db.Query<SanPham>("select top 5 * from SanPham where MaHangSanXuat = @0 and BiXoa =0 order by SoLuongBan Desc",id);
       }
       public static IEnumerable<SanPham> ListTop5ByType(int id)
       {
           var db = new WineStoreConnectionDB();
           return db.Query<SanPham>("select top 5 * from SanPham where MaLoaiSanPham = @0 and BiXoa =0 order by SoLuongBan Desc", id);
       }
        public static PetaPoco.Page<SanPham> ListByProducer(int id,int page){
           var db = new WineStoreConnectionDB();
            var result=db.Page<SanPham>(page, 12, // <-- page number and items per page

        "SELECT * FROM SanPham WHERE MaHangSanXuat=@0  and BiXoa =0 ORDER BY NgayLap DESC", id);

            return result;
       }
        public static PetaPoco.Page<SanPham> ListAllProduct( int page)
        {
            var db = new WineStoreConnectionDB();
            var result = db.Page<SanPham>(page, 12, // <-- page number and items per page

        "SELECT * FROM SanPham WHERE  BiXoa =0 ORDER BY NgayLap DESC");

            return result;
        }
        public static PetaPoco.Page<SanPham> ListByTypeForPage(int id,int page)
        {
            var db = new WineStoreConnectionDB();
            var result = db.Page<SanPham>(page, 12, // <-- page number and items per page

        "SELECT * FROM SanPham WHERE MaLoaiSanPham=@0 and BiXoa =0 ORDER BY NgayLap DESC", id);

            return result;
        }
        public static void Edit(SanPham sp)
        {
            var db = new WineStoreConnectionDB();

            StringBuilder sql = new StringBuilder();
            sql.Append("update SanPham set TenSanPham = N'");
            sql.Append(sp.TenSanPham);
            sql.Append("', GiaSanPham = ");
            sql.Append(sp.GiaSanPham);
            sql.Append(", NgayLap = '");
            sql.Append(((DateTime)sp.NgayLap).ToShortDateString());
            sql.Append("', XuatXu = '");
            sql.Append(sp.XuatXu);
            sql.Append("', BiXoa = ");
            if (sp.BiXoa == true) sql.Append(1);
            else sql.Append(0);
            sql.Append(", SoLuongTon = ");
            sql.Append(sp.SoLuongTon);
            sql.Append(", MaLoaiSanPham = ");
            sql.Append(sp.MaLoaiSanPham);
            sql.Append(", MaHangSanXuat = ");
            sql.Append(sp.MaHangSanXuat);
            sql.Append(", MoTa = N'");
            sql.Append(sp.MoTa);
            if (sp.HinhURL1 != null)
            {
                sql.Append("', HinhURL1 = '");
                sql.Append(sp.HinhURL1);
            }
            if (sp.HinhURL2 != null)
            {
                sql.Append("', HinhURL2 = '");
                sql.Append(sp.HinhURL2);
            }
            if (sp.HinhURL3 != null)
            {
                sql.Append("', HinhURL3 = '");
                sql.Append(sp.HinhURL3);
            }
            sql.Append("' where MaSanPham = ");
            sql.Append(sp.MaSanPham);

            db.Execute(sql.ToString());
        }
        public static void Create(SanPham sp)
        {
            var db = new WineStoreConnectionDB();
            //db.Execute("insert into SanPham('TenSanPham', 'HinhURL1', 'GiaSanPham', 'NgayLap', 'XuatXu', 'SoLuongTon', 'LoaiSanPham', 'HangSanXuat') values(@0, @1, @2, @3, @4, @5, @6, @7)", sp.TenSanPham, sp.HinhURL1, sp.GiaSanPham, sp.NgayLap, sp.XuatXu, sp.SoLuongTon, sp.MaHangSanXuat);
            //db.Execute("insert into SanPham() values(@0, @1, @2, @3, @4, @5, 0, 0, '', 0, @6, @7, '', '')", sp.TenSanPham, sp.HinhURL1, sp.GiaSanPham, sp.NgayLap, sp.XuatXu, sp.SoLuongTon, sp.MaLoaiSanPham, sp.MaHangSanXuat);
            sp.SoLuotXem = 0;
            sp.SoLuongBan = 0;
            sp.BiXoa = false;
            db.Insert("SanPham", "MaSanPham", sp);
        }
        public static void Update(SanPham tk)
        {
            var db = new WineStoreConnectionDB();
            db.Update("SanPham", "MaSanPham", tk);

        }
        public static int AutoID()
        {
            var db = new WineStoreConnectionDB();
            int a = db.SingleOrDefault<int>("select top 1 MaSanPham from SanPham order by MaSanPham desc");
            if (a == null)
            {
                a = 0;

            }
            return a + 1;



        }
    
    
    }
}
