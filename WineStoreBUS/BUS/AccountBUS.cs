﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;

namespace WineStoreBUS.BUS
{
    public class AccountBUS
    {
     
        public static bool CheckAccoutByEmail(AspNetUser tk)
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<AspNetUser> data = db.Query<AspNetUser>("Select * from AspNetUsers where Email = @0 and Id!=@1", tk.Email, tk.Id);
            if (data.Count() > 0)
            {
                return true;
            }
            return false;
        }
        public static AspNetUser LoadUserById(string  id)
        {
            var db = new WineStoreConnectionDB();
            AspNetUser data = db.SingleOrDefault<AspNetUser>("Select * from AspNetUsers where Id=@0",id);
            return data;
        }
        public static string LoadIdRolesAdmin()
        {
            var db = new WineStoreConnectionDB();
            string data = db.SingleOrDefault<string>("select Id from AspNetRoles  where Name = 'Admin'");
            return data;
        }
        public static void Update(AspNetUser tk)
        {
            var db = new WineStoreConnectionDB();
            db.Update("AspNetUsers", "Id", tk);
            
        }
        public static IEnumerable<AspNetUser> ListAccount()
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<AspNetUser> data = db.Query<AspNetUser>("Select * from AspNetUsers ");
          
            return data;
        }
        public static bool CheckRoles(AspNetUser tk)
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<AspNetUserRole> data = db.Query<AspNetUserRole>("Select UserId,RoleId from AspNetUserRoles s,AspNetRoles r where s.RoleId=r.Id and r.Name = 'Admin'and UserId=@0", tk.Id);
            if (data.Count() > 0)
            {
                return true;
            }
            return false;
        }
        public static void UploadRolesAdmin(AspNetUserRole tk)
        {
            var db = new WineStoreConnectionDB();
            try
            {

                
                db.Insert("AspNetUserRoles", tk);
            }catch(Exception e){


            }
         
           
        }
        public static void DelRoles(AspNetUserRole tk)
        {
            var db = new WineStoreConnectionDB();
            try
            {


                db.Delete("AspNetUserRoles", "UserId", tk);
            }
            catch (Exception e)
            {


            }


        }
     
    }
}
