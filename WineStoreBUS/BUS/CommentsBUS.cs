﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;
namespace WineStoreBUS.BUS
{
  public  class CommentsBUS
    {
        public static PetaPoco.Page<Comment> ListSearchFound(int idproduct, int page)
        {

            var db = new WineStoreConnectionDB();

      
            string sql = string.Format("select * from Comments where IdProduct ={0}",idproduct);

            var result = db.Page<Comment>(page, 10, sql);

            return result;
        }
        public static int AutoID()
        {
            var db = new WineStoreConnectionDB();
            int a = db.SingleOrDefault<int>("select top 1 IdComment from Comments order by IdComment desc");
            if (a == null)
            {
                a = 0;

            }
            return a + 1;



        }
        public static void Create(Comment sp)
        {
            var db = new WineStoreConnectionDB();
            int id = AutoID();
            sp.IdComment = id;
            db.Insert("Comments", "IdComment", sp);
        }
    }
}
