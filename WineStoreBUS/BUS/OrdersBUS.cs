﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;
namespace WineStoreBUS.BUS
{
    public class OrdersBUS
    {
        public static int AutoID()
        {
            var db = new WineStoreConnectionDB();
            int a = db.SingleOrDefault<int>("select top 1 MaDonDatHang from DonDatHang order by MaDonDatHang desc");
            if (a == null)
            {
                a = 0;

            }
            return a + 1;



        }
        public static string LoadTinhTrang(int id)
        {
            var db = new WineStoreConnectionDB();
            string a = db.SingleOrDefault<string>("select  TenTinhTrang from TinhTrang where MaTinhTrang = @0",id);
        
            return a;



        }
        public static IEnumerable<DonDatHang> LoadHistory(string id)
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<DonDatHang> a = db.Query<DonDatHang>("select * from DonDatHang where MaTaiKhoan=@0 order by NgayLap desc",id);

            return a;



        }
       

        public static void Insert(DonDatHang order)
        {

            var db = new WineStoreConnectionDB();


            db.Insert("DonDatHang", order);
        }
        public static string CreateIDCode(string strID, string field, string table, int length)
        {
            try
            {

                var db = new WineStoreConnectionDB();

                string IDCode = strID + "0000000000";
                string query = "select max(" + field.Trim() + ") from " + table.Trim() + " where left(" + field.Trim() + "," + strID.Length.ToString() + ")='" + strID.Trim() + "'";


                string a = db.SingleOrDefault<string>(query);
              
                IDCode = Convert.ToString(a);
                

                IDCode = IDCode.Substring(strID.Length);
                IDCode = "0000000000000000" + Convert.ToString(Convert.ToInt64(IDCode) + 1);
                IDCode = strID + IDCode.Substring(IDCode.Length - length + strID.Length);

              
                return IDCode;
            }
            catch (Exception ex)
            {
             
                throw ex;
            }
        }
          
    }
}
