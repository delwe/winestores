﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WineStoreConnection;

namespace WineStoreBUS.BUS
{
    public class DDHBUS
    {
        public static void Upload(DonDatHang order)
        {

            var data = new WineStoreConnectionDB();
            data.Update("DonDatHang", "MaDonDatHang", order);

        }
        public static string LoadName(string id)
        {
            var db = new WineStoreConnectionDB();
            AspNetUser loadName = db.SingleOrDefault<AspNetUser>("Select Name from AspNetUsers where Id=@0", id);
            string name = loadName.Name;
            return name;
        }
        public static DonDatHang LoadOrderById(string id)
        {
            var db = new WineStoreConnectionDB();
            DonDatHang loadName = db.SingleOrDefault<DonDatHang>("Select * from DonDatHang where MaDonDatHang=@0", id);
           
            return loadName;
        }
        public static string LoadTenSP(int id)
        {
            var db = new WineStoreConnectionDB();
            SanPham loadName = db.SingleOrDefault<SanPham>("Select TenSanPham from SanPham where MaSanPham=@0", id);
            string name = loadName.TenSanPham;
            return name;
        }
        public static string LoadHinhSP(int id)
        {
            var db = new WineStoreConnectionDB();
            SanPham loadName = db.SingleOrDefault<SanPham>("Select HinhURL1 from SanPham where MaSanPham=@0", id);
            string name = loadName.HinhURL1;
            return name;
        }      
        public static IEnumerable<TinhTrang> List()
        {
            var db = new WineStoreConnectionDB();
            return db.Query<TinhTrang>("Select * from TinhTrang");
        }
    }
}
