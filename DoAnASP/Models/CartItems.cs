﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoAnASP.Models
{
    public class CartItems
    {
        public WineStoreConnection.SanPham Product{ get;set;}
        public int Quantity { get; set; }
    }
}