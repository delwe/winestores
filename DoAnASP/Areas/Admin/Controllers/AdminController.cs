﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS;
using WineStoreConnection;
using WineStoreBUS.BUS;
using Admin.ViewModel;
namespace DoAnASP.Areas.Admin.Controllers
{
    
    public class AdminController : Controller
    {
        //
        // GET: /Admin/Admin/
        [Authorize(Roles="Admin")]
       public ActionResult Index()
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<SanPham> dssp = db.Query<SanPham>("Select * from SanPham");
            return View(dssp);
        }

        // GET: SP/Details/5
        [Authorize(Roles="Admin")]
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: SP/Create
        [Authorize(Roles="Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: SP/Create
        
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(ProductViewModels collection)
        {


            var validImageTypes = new string[]
            {
                    "image/gif",
                    "image/jpeg",
                    "image/pjpeg",
                    "image/png"
            };
            if (collection.ImageUpLoad == null || collection.ImageUpLoad0 == null || collection.ImageUpLoad1 == null || collection.ImageUpLoad.ContentLength == 0 || collection.ImageUpLoad0.ContentLength == 0 || collection.ImageUpLoad1.ContentLength == 0)
            {
                ModelState.AddModelError("ImageUpload", "This field is required");
            }
            else if (!validImageTypes.Contains(collection.ImageUpLoad.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            }


            var uploadDir = "~/Access/img/";

            if (collection.ImageUpLoad != null && collection.ImageUpLoad.ContentLength > 0)
            {
                string time = DateTime.Now.Ticks.ToString();
                var imagePath = Path.Combine(Server.MapPath(uploadDir), time + collection.ImageUpLoad.FileName);

                //var imageUrl = Path.Combine(uploadDir, sanpham.ImageUpLoad.FileName);   
                collection.ImageUpLoad.SaveAs(imagePath);
                collection.HinhURL1 = "img/" + time + collection.ImageUpLoad.FileName; // day la noi lay anh up len -> 
            }
            if (collection.ImageUpLoad0 != null && collection.ImageUpLoad0.ContentLength > 0)
            {
                string time = DateTime.Now.Ticks.ToString();
                var imagePath = Path.Combine(Server.MapPath(uploadDir), time + collection.ImageUpLoad0.FileName);

                //var imageUrl = Path.Combine(uploadDir, sanpham.ImageUpLoad.FileName);   
                collection.ImageUpLoad0.SaveAs(imagePath);
                collection.HinhURL2 = "img/" + time + collection.ImageUpLoad0.FileName; // day la noi lay anh up len -> 
            }
            if (collection.ImageUpLoad1 != null && collection.ImageUpLoad1.ContentLength > 0)
            {
                string time = DateTime.Now.Ticks.ToString();
                var imagePath = Path.Combine(Server.MapPath(uploadDir), time + collection.ImageUpLoad1.FileName);

                //var imageUrl = Path.Combine(uploadDir, sanpham.ImageUpLoad.FileName);   
                collection.ImageUpLoad1.SaveAs(imagePath);
                collection.HinhURL3 = "img/" + time + collection.ImageUpLoad1.FileName; // day la noi lay anh up len -> 
            }
            ProductBUS.Create(collection);
            return RedirectToAction("Index");
        }

        // GET: SP/Edit/5 
        [Authorize(Roles="Admin")]
        public ActionResult Edit(int id)
        {
            var db = new WineStoreConnectionDB();
            SanPham sp = db.SingleOrDefault<SanPham>("Select * from SanPham where MaSanPham = " + id);
            ProductViewModels spp = new ProductViewModels();

            spp.BiXoa = sp.BiXoa;
            spp.GiaSanPham = sp.GiaSanPham;
            spp.HinhURL1 = sp.HinhURL1;
            spp.HinhURL2 = sp.HinhURL2;
            spp.HinhURL3 = sp.HinhURL3;
            spp.MaLoaiSanPham = sp.MaLoaiSanPham;
            spp.MaHangSanXuat = sp.MaHangSanXuat;
            spp.MaSanPham = sp.MaSanPham;
            spp.MoTa = sp.MoTa;
            spp.NgayLap = sp.NgayLap;
            spp.SoLuongBan = sp.SoLuongBan;
            spp.SoLuongTon = sp.SoLuongTon;
            spp.SoLuotXem = sp.SoLuotXem;
            spp.TenSanPham = sp.TenSanPham;
            spp.XuatXu = sp.XuatXu;

            return View(spp);
        }

        // POST: SP/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: SP/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: SP/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Save(ProductViewModels collection)
        {
            var validImageTypes = new string[]
              {
                    "image/gif",
                    "image/jpeg",
                    "image/pjpeg",
                    "image/png"
              };
            if (collection.ImageUpLoad == null || collection.ImageUpLoad0 == null || collection.ImageUpLoad1 == null || collection.ImageUpLoad.ContentLength == 0 || collection.ImageUpLoad0.ContentLength == 0 || collection.ImageUpLoad1.ContentLength == 0)
            {
                ModelState.AddModelError("ImageUpload", "This field is required");
            }
            else if (!validImageTypes.Contains(collection.ImageUpLoad.ContentType))
            {
                ModelState.AddModelError("ImageUpload", "Please choose either a GIF, JPG or PNG image.");
            }


            var uploadDir = "~/Access/img/";

            if (collection.ImageUpLoad != null && collection.ImageUpLoad.ContentLength > 0)
            {
                string time = DateTime.Now.Ticks.ToString();
                var imagePath = Path.Combine(Server.MapPath(uploadDir), time + collection.ImageUpLoad.FileName);

                //var imageUrl = Path.Combine(uploadDir, sanpham.ImageUpLoad.FileName);   
                collection.ImageUpLoad.SaveAs(imagePath);                
                collection.HinhURL1 = "img/" + time + collection.ImageUpLoad.FileName; // day la noi lay anh up len -> 
            }
            if (collection.ImageUpLoad0 != null && collection.ImageUpLoad0.ContentLength > 0)
            {
                string time = DateTime.Now.Ticks.ToString();
                var imagePath = Path.Combine(Server.MapPath(uploadDir), time + collection.ImageUpLoad0.FileName);

                //var imageUrl = Path.Combine(uploadDir, sanpham.ImageUpLoad.FileName);   
                collection.ImageUpLoad0.SaveAs(imagePath);
                collection.HinhURL2 = "img/" + time + collection.ImageUpLoad0.FileName; // day la noi lay anh up len -> 
            }
            if (collection.ImageUpLoad1 != null && collection.ImageUpLoad1.ContentLength > 0)
            {
                string time = DateTime.Now.Ticks.ToString();
                var imagePath = Path.Combine(Server.MapPath(uploadDir), time + collection.ImageUpLoad1.FileName);

                //var imageUrl = Path.Combine(uploadDir, sanpham.ImageUpLoad.FileName);   
                collection.ImageUpLoad1.SaveAs(imagePath);
                collection.HinhURL3 = "img/" + time + collection.ImageUpLoad1.FileName; // day la noi lay anh up len -> 
            }

            ProductBUS.Edit(collection);
            return RedirectToAction("Index");
        }
      
    }
	
}