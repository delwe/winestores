﻿using DoAnASP.Areas.Admin.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using WineStoreBUS.BUS;
using WineStoreConnection;

namespace DoAnASP.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class QLDDHController : Controller
    {
        // GET: Admin/QLDH
        public ActionResult Index()
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<DonDatHang> dsddh = db.Query<DonDatHang>("Select * from DonDatHang order by MaTinhTrang");
            return View(dsddh);
        }

        // GET: Admin/QLDH/Details/5
        public ActionResult Details(string id)
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<ChiTietDonDatHang> dsctddh = db.Query<ChiTietDonDatHang>("Select * from ChiTietDonDatHang where MaDonDatHang = @0", id);
           AspNetUser KH = db.SingleOrDefault<AspNetUser>("Select * from  AspNetUsers u, DonDatHang d where u.Id=d.MaTaiKhoan and MaDonDatHang = @0", id);
            ViewBag.MaDDH = id;
            ViewBag.Ten = KH.Name;
            ViewBag.DC = KH.Address;
            ViewBag.DT = KH.Phone;
            ViewBag.Email = KH.Email;
            return View(dsctddh);
        }

        // GET: Admin/QLDH/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/QLDH/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Admin/QLDH/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Admin/QLDH/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        [HttpPost]
        public JsonResult XLOrder(string UpOrder)
        {
           
            var jsonCart = new JavaScriptSerializer().Deserialize<Order>(UpOrder);
            var temp = DDHBUS.LoadOrderById(jsonCart.id);
            temp.MaTinhTrang = jsonCart.TinhTrang;
            DDHBUS.Upload(temp);
           
            return Json(new
            {
                status = true
            });

        }

        // GET: Admin/QLDH/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Admin/QLDH/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
