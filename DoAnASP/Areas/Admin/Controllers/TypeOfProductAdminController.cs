﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS;
using WineStoreConnection;
using WineStoreBUS.BUS;

namespace DoAnASP.Areas.Admin.Controllers
{
    [Authorize(Roles="Admin")]
    public class TypeOfProductAdminController : Controller
    {
        //
        // GET: /Admin/TypeOfProductAdmin/
        public ActionResult Index()
        {
            var db = new WineStoreConnectionDB();
            IEnumerable<LoaiSanPham> dslsp = db.Query<LoaiSanPham>("Select * from LoaiSanPham");
            return View(dslsp);
        }

        // GET: LoaiSP/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LoaiSP/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LoaiSP/Create
        [HttpPost]
        public ActionResult Create(LoaiSanPham collection)
        {
            LoaiSPBUS.Create(collection);
            return RedirectToAction("Index");
        }

        // GET: LoaiSP/Edit/5
        public ActionResult Edit(int id)
        {
            var db = new WineStoreConnectionDB();
            LoaiSanPham lsp = db.SingleOrDefault<LoaiSanPham>("Select * from loaisanpham l where maloaisanpham = " + id);
            return View(lsp);
        }

        // POST: LoaiSP/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LoaiSP/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LoaiSP/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Save(LoaiSanPham collection)
        {
            LoaiSPBUS.Update(collection);
            return RedirectToAction("Index");
        }
	}
}