﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS.BUS;
using WineStoreConnection;
using Microsoft.AspNet.Identity;
namespace DoAnASP.Areas.Admin.Controllers
{
     [Authorize(Roles = "Admin")]
    public class AspNetUsersController : Controller
    {
        //
        // GET: /Admin/AspNetUsers/
        public ActionResult Index()
        {
           // QUan ly tai khoan 
        
            IEnumerable<AspNetUser> data = AccountBUS.ListAccount();
            return View(data);
        }
        public ActionResult Edit(string id)
        {
            AspNetUser user = AccountBUS.LoadUserById(id);

            return View(user);
        }
        [HttpPost]
        public ActionResult Edit(AspNetUser user,FormCollection form)
        {
            AspNetUser u = AccountBUS.LoadUserById(user.Id);
         

              if(AccountBUS.CheckAccoutByEmail(user) == true)
                    {

                        ViewBag.ThongBao = 2;
                        return View(u);

                    }
                    else
                    {
                        u.UserName = user.Email;
                        u.Email = user.Email;
                        u.Name = user.Name;
                        u.Phone = user.Phone;
                        u.Address = user.Address;
                        if (user.Id != User.Identity.GetUserId())
                        {
                            u.BiXoa = user.BiXoa;
                        }
                        AccountBUS.Update(u);
                        ViewBag.ThongBao = 1;
                    }
            
              if (user.Id != User.Identity.GetUserId())
              {

                  //

                  bool admin = false;
                  if (form["chkAdmin"] != null)
                  {
                      admin = true;
                  }

                  if (admin)
                  {
                      if (!AccountBUS.CheckRoles(user))
                      {
                          AspNetUserRole uroles = new AspNetUserRole();
                          uroles.RoleId = AccountBUS.LoadIdRolesAdmin();
                          uroles.UserId = user.Id;
                          AccountBUS.UploadRolesAdmin(uroles);
                          ViewBag.ThongBao = 1;

                      }

                  }
                  else
                  {
                      if (AccountBUS.CheckRoles(user))
                      {
                          AspNetUserRole uroles = new AspNetUserRole();
                          uroles.RoleId = AccountBUS.LoadIdRolesAdmin();
                          uroles.UserId = user.Id;
                          AccountBUS.DelRoles(uroles);
                          ViewBag.ThongBao = 1;

                      }

                  }
              }
              else
              {

                  ViewBag.ThongBao = 3;
                  return View(u);
              }

            return View(u);
        }
	}
}