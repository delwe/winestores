﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WineStoreConnection;
using WineStoreBUS;

namespace Admin.ViewModel
{
    public class ProductViewModels : SanPham
    {
        [Display(Name = "Hình ảnh")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase ImageUpLoad { get; set; }
        public HttpPostedFileBase ImageUpLoad0 { get; set; }
        public HttpPostedFileBase ImageUpLoad1 { get; set; }
    }
}