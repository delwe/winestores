﻿var comment =  {

    
    init: function () {
        comment.AddCommentsEvents();
    },
    AddCommentsEvents: function () {

        $('#submitComment').off('click').on('click', function () {
          
            if ($("#addComment").val() == null)
            {
                alert("Bạn phải điền vào ô bình luận ");
                return false;
            }
         

            var data = {
             
                Comments: $("#addComment").val(),
                UploadMedia: $("#uploadMedia").val(),
                IdProduct:$("#IdProduct").val()
             
            };
            $.ajax({
                url: "/Comments/AddComment/",
                type: "POST",
                data:{CommentModel: JSON.stringify(data)},
                dataType: "json",
           
                success: function (status) {
                    
                    if (status.Success == true) {

                        window.location.href = "/Product/Detail/" + $("#IdProduct").val()
                    } 
                },
                error: function () {
                    $("#message").html("Error while authenticating  user credentials!");
                }
            });
           

        });
        }
}
comment.init();