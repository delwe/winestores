﻿
jQuery(document).ready(function ($) {
	var formModal = $('.cd-user-modal'),
		formLogin = formModal.find('#cd-login'),
		formSignup = formModal.find('#cd-signup'),
		formForgotPassword = formModal.find('#cd-reset-password'),
		formModalTab = $('.cd-switcher'),
		tabLogin = formModalTab.children('li').eq(0).children('a'),
		tabSignup = formModalTab.children('li').eq(1).children('a'),
		forgotPasswordLink = formLogin.find('.cd-form-bottom-message a'),
		backToLoginLink = formForgotPassword.find('.cd-form-bottom-message a'),
		mainNav = $('.header-area');

	//open modal
	mainNav.on('click', function(event){
		$(event.target).is(mainNav) && mainNav.children('ul').toggleClass('is-visible');
	});

	//open sign-up form
	mainNav.on('click', '.cd-signup', signup_selected);
	//open login-form form
	mainNav.on('click', '.cd-signin', login_selected);

	//close modal
	formModal.on('click', function(event){
		if( $(event.target).is(formModal) || $(event.target).is('.cd-close-form') ) {
			formModal.removeClass('is-visible');
		}	
	});
	//close modal when clicking the esc keyboard button
	$(document).keyup(function(event){
    	if(event.which=='27'){
    		formModal.removeClass('is-visible');
	    }
    });

	//switch from a tab to another
	formModalTab.on('click', function(event) {
		event.preventDefault();
		( $(event.target).is( tabLogin ) ) ? login_selected() : signup_selected();
	});

	//hide or show password
	

	//show forgot-password form 
	forgotPasswordLink.on('click', function(event){
		event.preventDefault();
		forgot_password_selected();
	});

	//back to login from the forgot-password form
	backToLoginLink.on('click', function(event){
		event.preventDefault();
		login_selected();
	});

	function login_selected(){
		mainNav.children('ul').removeClass('is-visible');
		formModal.addClass('is-visible');
		formLogin.addClass('is-selected');
		formSignup.removeClass('is-selected');
		formForgotPassword.removeClass('is-selected');
		tabLogin.addClass('selected');
		tabSignup.removeClass('selected');
	}

	function signup_selected(){
		mainNav.children('ul').removeClass('is-visible');
		formModal.addClass('is-visible');
		formLogin.removeClass('is-selected');
		formSignup.addClass('is-selected');
		formForgotPassword.removeClass('is-selected');
		tabLogin.removeClass('selected');
		tabSignup.addClass('selected');
	}

	function forgot_password_selected(){
		formLogin.removeClass('is-selected');
		formSignup.removeClass('is-selected');
		formForgotPassword.addClass('is-selected');
	}
	$("#message").hide();
//	REMOVE THIS - it's just to show error messages 
	$("#btnLogin").on("click",function(){
       
		var loginemail = $('#signin-email').val().toString();
		var loginpd = $('#signin-password').val().toString();
                var flag = true;
                $("#notification").hide();
		if (loginemail=="") {
			$("#signin-email").toggleClass('has-error').next('span').toggleClass('is-visible');
                        flag = false;
                         $("#notification").hide();

		};
		if (loginpd == "" ||  loginpd.length<8) {

			$('#signin-password').toggleClass('has-error').next('span').toggleClass('is-visible');
                         flag = false;    
		};
	
	
                return flag;
	});
	$("#btnDangKy").on("click", function () {

	    var name = $('#signup-username').val().toString();
	    var Email = $('#signup-email').val().toString();
	    var Password = $('#signup-password').val().toString();
	    var confignpasswd = $('#signup-confirmpwd').val().toString();
	    var flag = true;
	   
	    if (name == "") {
	        $("#signup-username").toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;
	    
	    };
	    if (Password == "" || Password.length < 8) {

	        $('#signup-password').toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;
	    };
	    if( Email=="") {

	        $('#signup-email').toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;
	    };

	    if (Password != confignpasswd) {

	        $('#signup-confirmpwd').toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;
	    };

	    return flag;
	});
        
       
	$("#btnCheckout").on("click", function () {
                 var address = $('#useraddress').val().toString();
                var   email = $('#emailpro').val().toString();
                 
		var   name = $('#nameuser').val().toString();
		var   phone= $('#phonenumber').val().toString();
                
                var flag = true;
		if (address == "") {
              
                      $('#useraddress').toggleClass('has-error').next('span').toggleClass('is-visible');
			//formSignup.find('input[type="text"]').toggleClass('has-error').next('span').toggleClass('is-visible');
                          flag = false;

		};
		if (name == "") {
                 $('#nameuser').toggleClass('has-error').next('span').toggleClass('is-visible');
                   flag = false;

		};

		if (phone =="") {

		$('#phonenumber').toggleClass('has-error').next('span').toggleClass('is-visible');
                  flag = false;
		};
                if (email =="") {

		$('#emailpro').toggleClass('has-error').next('span').toggleClass('is-visible');
                  flag = false;
		};
              
            return flag;
		
	});
	$("#btnPay").on("click", function () {
	    var address = $('#useraddress').val().toString();
	 

	    var name = $('#nameuser').val().toString();
	    var phone = $('#phonenumber').val().toString();

	    var flag = true;
	    if (address == "") {

	        $('#useraddress').toggleClass('has-error').next('span').toggleClass('is-visible');
	        //formSignup.find('input[type="text"]').toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;

	    };
	    if (name == "") {
	        $('#nameuser').toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;

	    };

	    if (phone == "") {

	        $('#phonenumber').toggleClass('has-error').next('span').toggleClass('is-visible');
	        flag = false;
	    };
	  
	    return flag;

	});
       $("#btnOK").on("click",function (){


		var oldp = $('#oldpassword').val().toString();
              
		var newp = $('#newpassword').val().toString();
		var cofigp = $('#configpassword').val().toString();
              
                var flag = true;
		if (oldp == "") {
                      $('#oldpassword').toggleClass('has-error').next('span').toggleClass('is-visible');
			//formSignup.find('input[type="text"]').toggleClass('has-error').next('span').toggleClass('is-visible');
                          flag = false;

		};
		if (newp == "" || newp.length<8) {
                 $('#newpassword').toggleClass('has-error').next('span').toggleClass('is-visible');
                   flag = false;

		};

		
                if (cofigp != newp || cofigp==""){
                    $('#configpassword').toggleClass('has-error').next('span').toggleClass('is-visible');
                      flag = false;
                    
                };
                    
            return flag;
		
	});
        

    
	//IE9 placeholder fallbac
	//credits http://www.hagenburger.net/BLOG/HTML5-Input-Placeholder-Fix-With-jQuery.html
	if(!Modernizr.input.placeholder){
		$('[placeholder]').focus(function() {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
		  	}
		}).blur(function() {
		 	var input = $(this);
		  	if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.val(input.attr('placeholder'));
		  	}
		}).blur();
		$('[placeholder]').parents('form').submit(function() {
		  	$(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
			 		input.val('');
				}
		  	})
		});
	}

});


//credits http://css-tricks.com/snippets/jquery/move-cursor-to-end-of-textarea-or-input/
jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
    	// If this function exists...
    	if (this.setSelectionRange) {
      		// ... then use it (Doesn't work in IE)
      		// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
      		var len = $(this).val().length * 2;
      		this.focus();
      		this.setSelectionRange(len, len);
    	} else {
    		// ... otherwise replace the contents with itself
    		// (Doesn't work in Google Chrome)
      		$(this).val($(this).val());
    	}
	});
};