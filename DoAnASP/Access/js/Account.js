﻿var Account = {
    init: function () {

        Account.LoginEvents();
        Account.SignupEvents();
    },
    LoginEvents: function () {

        $('#btnLogin').off('click').on('click',function () {
          
            var data = {
             
                Email: $("#signin-email").val(),
                Password: $("#signin-password").val(),
                Rememberme: $("#remember-me").prop("checked")
            };
            $.ajax({
                url: "/Account/ValidateUser/",
                type: "POST",
                data:{LoginModel: JSON.stringify(data)},
                dataType: "json",
           
                success: function (status) {
                    
                    if (status.Success == true) {
                       window.location.href = "/"
                    } else {

                        $("#mssgerror").show();
                        flag = false;
                    }    
                },
                error: function () {
                    $("#message").html("Error while authenticating  user credentials!");
                }
            });
            return false;
        });
    },
    SignupEvents: function () {

        $('#btnDangKy').click(function () {
        
            var data = {
                Name: $("#signup-username").val(),
                Email: $("#signup-email").val(),
                Password: $("#signup-password").val()
  

            };
            $.ajax({
                url: "/Account/CreateAnAccount/",
                type: "POST",
                data: { ResigterModel: JSON.stringify(data) },
                dataType: "json",
                success: function (status) {

                    if (status.Success == true) {
                        window.location.href = "/"
                    
                    } else {
                
                        $('#signup-email').toggleClass('has-error').next('span').text("Email đã tồn tại ");
                        $('#signup-email').toggleClass('has-error').next('span').toggleClass('is-visible');
                     
                        

                    }


                },
                error: function () {
                    $("#message").html("Error while authenticating  user credentials!");
                }
            });
            return false;
        });
    }


}
Account.init();