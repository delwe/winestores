﻿var Cart = {

    init: function () {
        Cart.regEvents();
        Cart.delEvents();
    },

    delEvents:function(){

        $("a[id*='btnDelete_']").on('click', function () {
            var proId = $(this).data('proid');
            $.ajax({
                url: '/Cart/DeleteCart/',
                data: { cartModel: JSON.stringify(proId) },
                dataType: 'json',
                type: "POST",
                success: function (res) {

                    if (res.status == true) {
                        window.location.href = "/Cart"
                    } else {

                    }
                }

            });
        });


      

    },
    regEvents: function () {
        $("a[id*='btnUpdate_']").off("click").on('click', function () {
            var proId = $(this).data('proid');
            $('#txtUpdProId').val(proId);

            var q = $('#txtQuantity_' + proId).val();

          
            var Cart = {
                Quantity:q,
                Product:{
                    MaSanPham:proId
                }

            };

            
            $.ajax({
                url: '/Cart/Update/',
                data: { cartModel: JSON.stringify(Cart) },
                dataType: 'json',
                type: "POST",
                success: function (res) {

                    if (res.status == true)
                    {
                        window.location.href="/Cart"
                    } else {
                        
                    }
                }

            });
           
        });

    }
}
Cart.init();
