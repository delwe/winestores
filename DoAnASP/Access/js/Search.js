﻿var common = {
    init: function () {
        common.registerEven();
    },
    registerEven: function () {
        function log(message) {
            $("<div>").text(message).prependTo("#log");
            $("#log").scrollTop(0);
        }
        $("#txtSearch").autocomplete({
            minLength: 0,
            source: function( request, response ) {
                $.ajax({
                    url: "/Product/ListProduct",
                    dataType: "json",
                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        response(data.data);
                    }
                });
            },
            focus: function( event, ui ) {
                $("#txtSearch").val(ui.item.label);
                return false;
            },
            select: function( event, ui ) {
                $("#txtSearch").val(ui.item.label);
               
 
                return false;
            }
        })
     .autocomplete("instance")._renderItem = function (ul, item) {
         return $("<li>")
           .append("<a src='"+"/Product/Search?txtSearch="+item.label+"'>" + item.label + "</a>")
          
           .appendTo(ul);
     };
 

    }
}
common.init();