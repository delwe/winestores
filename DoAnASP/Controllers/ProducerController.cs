﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS.BUS;

namespace DoAnASP.Controllers
{
    public class ProducerController : Controller
    {
        //
        // GET: /Producer/
        public ActionResult Index(int id,int? page)
        {
            //Toan bo producers
            if (page == null)
                page = 1;
            PetaPoco.Page<WineStoreConnection.SanPham> ds_SP = ProductBUS.ListByProducer(id,page.Value);
            return View(ds_SP);
        }
       

        public ActionResult AllProducer()
        {
            
            return View(ProducerBUS.List());
        }
        
	}
}