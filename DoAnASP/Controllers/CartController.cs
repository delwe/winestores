﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DoAnASP.Models;
using WineStoreBUS.BUS;
using WineStoreConnection;
using System.Web.Script.Serialization;
using Microsoft.AspNet.Identity;
namespace DoAnASP.Controllers
{
    public class CartController : Controller
    {
        //
        // GET: /Cart/

        public ActionResult Index()
        {
            var cart = Session["Cart"];
            var list = new List<CartItems>();
            if(cart!=null)
            {

                list = (List<CartItems>)cart;
            }
            return View(list);
        }
        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            var cart = Session["Cart"];
            var list = new List<CartItems>();
            if (form["btnPay"] != null)
            {

                var userAddress = form["useraddress"].ToString();
                var Name = form["nameuser"].ToString();
                var phone = form["phonenumber"].ToString();
                if (!Request.IsAuthenticated)
                {
                    ViewBag.ThongBao = 3;
                    return View(list);
                }


                var db = new WineStoreConnectionDB();
                WineStoreConnection.AspNetUser user = db.SingleOrDefault<WineStoreConnection.AspNetUser>("Select * from AspNetUsers where Id=@0", User.Identity.GetUserId());
                user.Address = userAddress;
                user.Phone = phone;
                user.Name = Name;
                AccountBUS.Update(user);
                
                if (cart != null)
                {
                    var total = int.Parse(form["txtTotal"].ToString());

                    list = (List<CartItems>)cart;
                    DonDatHang a = new DonDatHang();
                    a.MaTaiKhoan = User.Identity.GetUserId();
                    a.MaTinhTrang = 1;
                    a.NgayLap = DateTime.Now;
                    a.TongThanhTien = total;

                   String idOd = OrdersBUS.CreateIDCode("DH","MaDonDatHang","DonDatHang",8);
                   a.MaDonDatHang = idOd;
                   OrdersBUS.Insert(a);
                    foreach(var item in list)
                    {
                        ChiTietDonDatHang detail = new ChiTietDonDatHang();
                        detail.MaChiTietDonDatHang = OrderDetails.AutoID();
                        detail.MaDonDatHang = idOd;
                        detail.SoLuong = item.Quantity;
                        detail.GiaBan = item.Product.GiaSanPham;
                        detail.ThanhTien = item.Product.GiaSanPham * item.Quantity;
                        detail.MaSanPham = item.Product.MaSanPham;

                        SanPham temp = item.Product;
                        temp.SoLuongBan = temp.SoLuongBan + item.Quantity;
                        temp.SoLuongTon = temp.SoLuongTon - item.Quantity;
                        ProductBUS.Update(temp);

                        OrderDetails.Insert(detail);
                    }
                    Session["Cart"] = null;
                    list = new List<CartItems>();

                    ViewBag.ThongBao = 1;


                  

                }



            }
            
          
            return View(list);
        }
        public ActionResult AddItems(int? Proid, int? quantity)
        {

           
            if (Proid == null)
            {

                Redirect(Url.Action("Index", "Home"));
            }
            if (quantity == null)
            {

                quantity = 1;
            }
            var temp = ProductBUS.GetbyID(Proid.Value);
            var cart = Session["Cart"];
            if (cart != null)
            {
                var list = (List<CartItems>)cart;
                if (list.Exists(m => m.Product.MaSanPham == Proid))
                {
                    foreach (var item in list)
                    {

                        if (item.Product.MaSanPham == Proid)
                        {

                            item.Quantity += quantity.Value;
                        }
                    }

                }
                else
                {

                    var CartItem = new CartItems();
                    CartItem.Product = temp;
                    CartItem.Quantity = quantity.Value;

                    list.Add(CartItem);
                }
                Session["Cart"] = list;
            }
            else
            {
                var CartItem = new CartItems();
                CartItem.Product = temp;
                CartItem.Quantity = quantity.Value;
                var list = new List<CartItems>();
                list.Add(CartItem);
                Session["Cart"] = list;
            }


            return Redirect(Request.UrlReferrer.ToString());



        }
        [HttpPost]
        public ActionResult AddItemss(FormCollection form)
        {
            if (form["btnAddToCart"] != null)
            {
               
                var Proid = int.Parse(form["txtProIDCart"].ToString());
                var quantity = int.Parse(form["txtQuantity"].ToString());
                var temp = ProductBUS.GetbyID(Proid);
                var cart = Session["Cart"];
                if (cart != null)
                {
                    var list = (List<CartItems>)cart;
                    if (list.Exists(m => m.Product.MaSanPham == Proid))
                    {
                        foreach (var item in list)
                        {

                            if (item.Product.MaSanPham == Proid)
                            {

                                item.Quantity += quantity;
                            }
                        }

                    }
                    else
                    {

                        var CartItem = new CartItems();
                        CartItem.Product = temp;
                        CartItem.Quantity = quantity;

                        list.Add(CartItem);
                    }
                    Session["Cart"] = list;
                }
                else
                {
                    var CartItem = new CartItems();
                    CartItem.Product = temp;
                    CartItem.Quantity = quantity;
                    var list = new List<CartItems>();
                    list.Add(CartItem);
                    Session["Cart"] = list;
                }

                return Redirect(Url.Action("Detail", "Product", new { id = Proid }));
            }


            return Redirect("/");
        }
        [HttpPost]
        public JsonResult DeleteCart(string cartModel)
        {
            var cart = (List<CartItems>)Session["Cart"];
            var jsonCart = new JavaScriptSerializer().Deserialize<int>(cartModel);
            var temp = ProductBUS.GetbyID(jsonCart);

            foreach (var item in cart)
            {
                if (item.Product.MaSanPham == temp.MaSanPham)
                {
                    cart.Remove(item);
                    break;
                }
            }
            Session["Cart"] = cart;
            return   Json(new
            {
                status = true
            });

        }

            

        

        [HttpPost]
        public JsonResult Update(string cartModel)
        {
           
            var jsonCart = new JavaScriptSerializer().Deserialize<CartItems>(cartModel);
            var SessionCart = Session["Cart"];
           
             
                var temp = ProductBUS.GetbyID(jsonCart.Product.MaSanPham);

                var list = (List<CartItems>)SessionCart;
                    if (list.Exists(m => m.Product.MaSanPham == temp.MaSanPham))
                    {
                        foreach (var item in list)
                        {

                            if (item.Product.MaSanPham == temp.MaSanPham)
                            {

                                item.Quantity = jsonCart.Quantity;
                            }
                        }

                    }
                    Session["Cart"] = list;
                    return Json(new {
                        status = true
                    });
            
        }
	}
}