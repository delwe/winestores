﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS.BUS;
namespace DoAnASP.Controllers
{
    public class ProductController : Controller
    {
        //
        // GET: /Product/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Detail(int id,int? page)
        {
            if(page == null)
            {

                page = 1;
            }
            WineStoreConnection.SanPham sp = ProductBUS.GetbyID(id);
            string a = ProducerBUS.NameProducerByID(sp.MaHangSanXuat);
            string b =  LoaiSPBUS.NameLoaiSPByID(sp.MaLoaiSanPham);
            ViewBag.HangSanXuat = a;
            ViewBag.LoaiSanPham =b;
            ViewData["page"] = page.Value;
           
          
            return View(sp);
        }
        public ActionResult Search(string txtSearch,int? page)
        {
            if (page== null)
            {

                page = 1;
            }
            var data = ProductBUS.ListSearchFound(txtSearch, page.Value);
            if (data.Items.Count == 1)
            {
                return Redirect(Url.Action("Detail", "Product", new { id = data.Items.First().MaSanPham }));
            }
            return View(data);
        }

        public JsonResult ListProduct(string q)
        {
            IEnumerable<WineStoreConnection.SanPham> data = ProductBUS.ListSearch(q);
            var name = data.Select(m => m.TenSanPham).ToList();
            var img = data.Select(m => m.HinhURL1).ToList();
            var id = data.Select(m => m.MaSanPham).ToList();

            var json = Json( new{
                data = name,
           
                status = true
                
            },JsonRequestBehavior.AllowGet);

           
            return json;
        }
        public ActionResult Top5ByProducer(int id)
        {

            return View(ProductBUS.ListTop5ByProducer(id));
        }
        public ActionResult Top5ByType(int id)
        {

            return View(ProductBUS.ListTop5ByType(id));
        }
      
	}
}