﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS.BUS;
namespace DoAnASP.Controllers
{
    public class LoaiSPController : Controller
    {
        //
        // GET: /LoaiSP/
        public ActionResult Index(int id,int? page)
        {
            //Toan Bo Loai San Phamss
            if (page == null)
            {
                page = 1;
            }
            PetaPoco.Page<WineStoreConnection.SanPham> ds_SP = ProductBUS.ListByTypeForPage(id,page.Value);
            return View(ds_SP);
        }
	}
}