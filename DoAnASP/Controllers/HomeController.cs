﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS.BUS;
namespace DoAnASP.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult NewProduct()
        {
            return View(ProductBUS.ListNew());
        }
        public ActionResult SellingProduct()
        {
            return View(ProductBUS.ListSelling());
        }
        public ActionResult TopViewProduct()
        {
            return View(ProductBUS.ListTopView());
        }
        public ActionResult SearchBar()
        {
            //Tahnh Tim kimes
            return View();
        }

        public ActionResult Search(int? selectCat, int? selectManu, int? pricerange, int? page)
        {

            //search ajaxs
            if(page == null)
            {

                page = 1;
            }
            if (selectCat == null)
            {
                selectCat = 0;
            }
            if (selectManu == null)
            {
                selectManu = 0;
            }
            if (pricerange == null)
            {
                pricerange = 0;
            }
            ViewData["catid"] = selectCat.Value;
            ViewData["manuid"] = selectManu.Value;
            ViewData["price"] = pricerange.Value;

            

            return View(ProductBUS.SearchBar(selectCat.Value, selectManu.Value, pricerange.Value, page.Value));
        }

       
    }
}
