﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WineStoreBUS.BUS;
using WineStoreConnection;
using System.Web.Script.Serialization;
using DoAnASP.Models;
using Microsoft.AspNet.Identity;
namespace DoAnASP.Controllers
{
    public class CommentsController : Controller
    {
        //
        // GET: /Comments/
        public ActionResult Index(int idproduct,int page)
        {
          

            PetaPoco.Page < Comment > ListComments = CommentsBUS.ListSearchFound(idproduct, page);
            ViewData["idProduct"] = idproduct;
            return View(ListComments);
        }

        [HttpPost]
        public JsonResult AddComment(string CommentModel)
        {

            var jsonCart = new JavaScriptSerializer().Deserialize<CommentModel>(CommentModel);


          
            Comment cm = new Comment();
            cm.IdProduct = jsonCart.IdProduct ;
            cm.IdUser = User.Identity.GetUserId();
            cm.UrlMedia = jsonCart.UploadMedia;
            cm.DateComment = DateTime.Now;
            cm.BiXoa = false;
            cm._Comment = jsonCart.Comments;

            CommentsBUS.Create(cm);
            return Json(new
            {
                status = true
            });

        }
	}
}